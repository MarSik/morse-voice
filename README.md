# Generator of czech morse training audio tracks

The idea behind this tool is to allow learning and testing morse
reception training using audio only. For this audio tracks
can be generated that play morse symbols or groups and accompany
that with the message in spoken form.

It is possible to customize the ordering of morse vs spoken form and
the length of symbol groups and their count.

One of the modes can also generate multiple audio "flashcards" each with one morse symbol and its spoken form. These are good for audio players in shuffle or randomized mode.

The speech is generated using very crude diphone based algorithm and a voice data taken from the Czech festival voice files by Brailcom.

## Usage

- clone the repository
- run `git submodule update --init` to get the voice files
- run `pipenv install` to get the necessary libraries
- run `pipenv run python3 festival_morse.py --<mode> <ouput_dir>` where `mode` is random, cards or lesson.

There are many arguments you can pass to the tool to customize the generator:

```
Usage:
  festival_morse.py [options] [<dist>]

Options:
  -v <VOICE> --voice=<VOICE>        Root directory with the czech voice files [default: voice-czech-ph]
  -p <PITCH> --pitch=<PITCH>        Morse tone frequency [default: 700]
  -s <SPEED> --speed=<SPEED>        Speed of morse in WPM [default: 20]
  -f <SPEED> --farnsworth=<SPEED>   Farnsworth timing / effective speed [default: 12]

  --cards                   Generate one file for every sound in dist directory
  --random=<N>              Generate N random groups
  --group=<LENGTH>          Group length [default: 5]
  --symbols=<LIST>          Space separated list of symbols for random and card generators
  --lesson=<N>              Select symbols based on lesson number

  --text=<TEXT>             Generate one file with the text

  --repeat=<N>              Repeat morse N times [default: 1]
  -m --morse                Only generate morse, no voice
  --voice-first             Add spoken version first and morse second
```

## Hints

### Generate lessons based on Koch ordering and convert all files to m4a aac

```
for l in $(seq -w 1 41); do
  name=$(pipenv run python3 festival_morse.py --lesson=$l --koch --print-lesson-name)
  pipenv run python3 festival_morse.py --cards --lesson=$l --koch lessons/lesson$l;
  find lessons/lesson$l -name '*.wav' -exec \
    sh -c "faac --overwrite --album \"Morse to voice course\" --artist \"OK7MS\" --title \"Morse lesson $l - $name\" -b 128 -o lessons/lesson$l-\$0.m4a \$0; rm \$0;" {} \;
done
```

### Generate lesson with random length groups as a single long audio file

```
pipenv run python3 festival_morse.py --random=3000 --group=1-3 --lesson=5 --koch --prefer-new
```

```
for l in $(seq -w 1 41); do
  name=$(pipenv run python3 festival_morse.py --lesson=$l --koch --print-lesson-name)
  pipenv run python3 festival_morse.py --random=3000 --group=1-3 --lesson=$l --koch --prefer-new lessons/lesson$l;
  find lessons/lesson$l -name '*.wav' -exec \
    sh -c "faac --overwrite --album \"Morse to voice course\" --artist \"OK7MS\" --title \"Morse lesson $l - $name\" -b 128 -o lessons/lesson$l-\$0.m4a \$0; rm \$0;" {} \;
done
```

## Authors and copying

The work is licensed under the Apache 2 license.
And the code was written by Martin Sivak OK7MS.

