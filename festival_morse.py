#!/usr/bin/python

"""
This script is able to convert Festival data and morse code to
audio file for morse practice session.

The default able is configured to prepare czech version
of alphabet practice and uses the diphone database from
the czech diphone database by Brailcom.

Usage:
  festival_morse.py [options] [<dist>]

Options:
  -v <VOICE> --voice=<VOICE>
      Root directory with the czech voice files
      [default: voice-czech-ph]
  -p <PITCH> --pitch=<PITCH>
      Morse tone frequency [default: 700]
  -s <SPEED> --speed=<SPEED>
      Speed of morse in WPM [default: 20]
  -f <SPEED> --farnsworth=<SPEED>
      Inter-character spacing speed, also known as Farnsworth
      or effective speed. [default: 12]

  --print-lesson-name
      Print the lesson symbol and quit
  --cards
      Generate one file for every sound in dist directory
  --random=<N>
      Generate N random groups
  --group=<LENGTH>
      Group length. Can be a fixed number or a range (eg: --group=1-5)
      [default: 5]
  --symbols=<LIST>
      Space separated list of symbols for random and card generators
  --phrases=<FILE>
      Generate random phrases from FILE. Each line in the FILE is one
      phrase. Spaces are stripped out and must be added explicitly.
      There are some special symbols available for detailed
      group generation:
        | (pipe) - group separator
        < (less than) - start of random selection group
        > (more than) - end of random selection group
        _ (underscore) - insert space
  --lesson=<N>
      Select symbols based on lesson number
  --prefer-new
      Prefer newer symbols in a lesson when generating random groups.
      This simply makes the symbols from recent lessons appear more often
      than the symbols from lessons learned long ago.
  --koch
      Use Koch based lesson ordering instead of alphabetical one.

  --text=<TEXT>
      Generate one file with the text

  --repeat=<N>
      Repeat each morse group N times [default: 1]
  -m --morse
      Only generate morse, no voice
  --voice-first
      Add spoken version first and morse second

  --verbose=<N>
      Verbosity level [default: 0]
"""

import sys
import struct
import math
import wave
import os.path
import random
import re
from collections import namedtuple

import docopt

from scipy.signal import butter, lfilter, cheby2, cheby1

verbosity = 0


def print_v(level, content):
    if verbosity > level:
        print(content)


MorseSymbol = namedtuple('MorseSymbol', ('text', 'voice', 'morse', 'name'))

MORSE_SPACE = MorseSymbol(' ', ' ', ' ', 'mezera')

# sounds to prepare
table = [
    #('id', 'text', 'morse') # diphones separated by /
    MorseSymbol('a', 'a:', '.-', 'pismeno-a'),
    MorseSymbol('b', 'b/e:', '-...', 'pismeno-b'),
    MorseSymbol('c', 'c/e:', '-.-.', 'picmeno-c'),
    MorseSymbol('d', 'd/e:', '-..', 'pismeno-d'),
    MorseSymbol('e', 'e:', '.', 'pismeno-e'),
    MorseSymbol('f', 'e/f', '..-.', 'pismeno-f'),
    MorseSymbol('g', 'g/e:', '--.', 'pismeno-g'),
    MorseSymbol('h', 'h/a:', '....', 'pismeno-h'),
    MorseSymbol('i', 'i:', '..', 'pismeno-i'),
    MorseSymbol('j', 'j/e:', '.---', 'pismeno-j'),
    MorseSymbol('k', 'k/a', '-.-', 'pismeno-k'),
    MorseSymbol('l', 'e/l', '.-..', 'pismeno-l'),
    MorseSymbol('m', 'e/m', '--', 'pismeno-m'),
    MorseSymbol('n', 'e/n', '-.', 'pismeno-n'),
    MorseSymbol('o', 'o:', '---', 'pismeno-o'),
    MorseSymbol('p', 'p/e:', '.--.', 'pismeno-p'),
    MorseSymbol('q', 'k/v/e:', '--.-', 'pismeno-q'),
    MorseSymbol('r', 'e/r', '.-.', 'pismeno-r'),
    MorseSymbol('s', 'e/s', '...', 'pismeno-s'),
    MorseSymbol('t', 't/e:', '-', 'pismeno-t'),
    MorseSymbol('u', 'u:', '..-', 'pismeno-u'),
    MorseSymbol('v', 'v/e:', '...-', 'pismeno-v'),
    MorseSymbol('w', 'd/v/o/j/i:/v/e:', '.--', 'pismeno-w'),
    MorseSymbol('x', 'i/k/s', '-..-', 'pismeno-x'),
    MorseSymbol('y', 'i/p/s/i/l/o/n', '-.--', 'pismeno-y'),
    MorseSymbol('z', 'z/e/t', '--..', 'pismeno-z'),
    MorseSymbol('1', 'j/e/d/n/a', '.----', 'cislo-1'),
    MorseSymbol('2', 'd/v/j/e', '..---', 'cislo-2'),
    MorseSymbol('3', 't/r~*/i', '...--', 'cislo-3'),
    MorseSymbol('4', 'c~/t/i/r~/i', '....-', 'cislo-4'),
    MorseSymbol('5', 'p/j/e/t', '.....', 'cislo-5'),
    MorseSymbol('6', 's~/e/s/t', '.----', 'cislo-6'),
    MorseSymbol('7', 's/e/d/m', '--...', 'cislo-7'),
    MorseSymbol('8', 'o/s/m', '---..', 'cislo-8'),
    MorseSymbol('9', 'd/e/v/j/e/t', '----.', 'cislo-9'),
    MorseSymbol('0', 'n/u/l/a', '-----', 'cislo-0'),
    MorseSymbol('.', 't/e/c~/k/a', '.-.-.-', 's-tecka'),
    MorseSymbol(',', 'c~/a:/r/k/a', '.-.-.-', 's-carka'),
    MorseSymbol('/', 'l/o/m/i:/t/k/o', '-..-.', 's-lomitko'),
    MorseSymbol('=', 'r/o/v/n/a:/_/s/e', '-...-', 's-rovnase'),
    MorseSymbol('-', 'p/o/m/l/c~/k/a', '-....-', 's-pomlcka'),
    MorseSymbol('+', 'p/l/u/s', '.-.-.-', 's-plus'),
    MorseSymbol('?', 'o/t/a/z/n~/i:/k', '..--..',  's-otaznik'),
    MorseSymbol('!', 'v/i/k/r~*/i/c~/n~/i:/k', '-.-.--', 's-vykricnik'),
    MorseSymbol('\xff', 'ch/i/b/a', '........', 'chyba')
    ]


mapa = {r.text: r for r in table}

koch = "k m u r e s n a p t l w i . j z = f o y , v g 5 / q 9 2 h 3 8 b ? 4 7 c 1 d 6 0 x".split()


def sample(width, rate, time):
    """compute input sample position based on framerate and time"""
    return int(round((width * rate) * time))


STRUCTFORMAT = (None, "b", "h", None, "i", None, None, None, "q")


def find_last_pos_zero(iterable, last = None):
    """Find the last positive zero crossing.
       Returns the index of the first sample after the zero crossing."""
    if last is None:
        last = iterable[-1]
    for idx, v in enumerate(reversed(iterable)):
        if v == 0 and last < 0:
            return len(iterable) - idx
        if math.copysign(1, v) > math.copysign(1, last):
            return len(iterable) - idx
        last = v

    return len(iterable)


def word(diphdb, text):
    """prepare one sound from diphone entry text (diphones are delimited by /)"""
    text = text.split('/') + ['#']
    samples = []
    lastch = "#"
    for ch in text:
        diphid = "%s-%s" % (lastch, ch)
        #print diphid
        diphrec = diphdb[diphid]
        f = wave.open(os.path.join(wavdir, "%s.wav" % diphrec[0]), "rb")
        samplerate = f.getframerate()
        samplewidth = f.getsampwidth()
        start = sample(1, samplerate, diphrec[1])
        end = sample(1, samplerate, diphrec[3])

        # start 500 samples before the middle position for zero crossing detection
        f.setpos(max(start - 500, 0))

        # find the last zero in the leading 500 samples for nice concatenation
        # read 500 + 1 to figure out the crossing direction of the last sample
        struct_format = "<%d%s" % (min(start, 501), STRUCTFORMAT[samplewidth])
        find_zero_samples = struct.unpack(struct_format,
                                          f.readframes(min(start, 501)))
        find_zero = find_last_pos_zero(find_zero_samples[:-1], find_zero_samples[-1])
        samples += find_zero_samples[find_zero:]

        # read samples, but leave last 500 for zero crossing detection
        # the first sample was added already by the leading zero detection
        samples_count = end - start - 1
        struct_format = "<%d%s" % (samples_count - 500, STRUCTFORMAT[samplewidth])
        samples += struct.unpack(struct_format, f.readframes(samples_count - 500))

        # find the last zero in the last 500 samples for nice concatenation
        # read one extra frame to find the crossing direction of the last sample
        following_samples = f.readframes(501)
        struct_format = "<%d%s" % (len(following_samples) / samplewidth,
                                   STRUCTFORMAT[samplewidth])
        find_zero_samples = struct.unpack(struct_format,
                                          following_samples)
        find_zero = find_last_pos_zero(find_zero_samples[:-1], find_zero_samples[-1])
        samples += find_zero_samples[:find_zero]

        f.close()
        lastch = ch
        if lastch == "_":
            lastch = '#'

    return samples


def dot_samples(tempo, samplerate):
    return (60.0 * samplerate) / (tempo * 50)


def word_space_samples(tempo, samplerate):
    return 7 * dot_samples(tempo, samplerate)


def morse(tempo, effective_tempo, morse_text, samplerate = 44100):
    # compute the number of samples for a dot
    dot = dot_samples(tempo, samplerate)

    out = []
    for idx, t in enumerate(morse_text):
        if t==".":
            out.append((True, dot))
            if idx < len(morse_text) - 1:
                out.append((False, dot))
        elif t=="-":
            out.append((True, 3*dot))
            if idx < len(morse_text) - 1:
                out.append((False, dot))
        elif t==" ":
            out.append((False, word_space_samples(effective_tempo, samplerate)))

    return out


def morse_audio(morse_samples, samplerate=44100, pitch=700, ampl=2**15):
    # resting value
    m_samples = []

    for output, length in morse_samples:
        if output:
            m_samples.extend(gen_sound(samplerate, pitch, length, ampl))
        else:
            m_samples.extend([0]*int(round(length)))

    # Add zeros to the end to remove the final click
    m_samples.extend([0]*samplerate)
    m_samples = butter_bandpass_filter(m_samples, pitch, 250, samplerate, order=5)
    # trim not needed excess zeros
    while m_samples[-1] == 0:
        m_samples = m_samples[:-1]

    return [int(round(s*ampl)) for s in m_samples]


def space(samplerate, length):
    return [0]*int(math.floor(length*samplerate))


def butter_bandpass(center, width, fs, order=5):
    nyq = 0.5 * fs
    low_cutoff = (center - width/2) / nyq
    high_cutoff = (center + width/2) / nyq
    #b, a = butter(order, (low_cutoff, high_cutoff), btype='bandpass', analog=False)
    b, a = cheby1(order, 0.1, (low_cutoff, high_cutoff), btype='bandpass', analog=False)
    return b, a


def butter_bandpass_filter(data, center, width, fs, order=5):
    b, a = butter_bandpass(center, width, fs, order=order)
    y = lfilter(b, a, data)
    return y


def gen_sound(samplerate, pitch, length, ampl):
    period = float(samplerate) / pitch
    # prepare sine wave centered around 0
    wave = [math.sin(math.pi*(2 * s / period))
               for s in range(int(round(length)))]

    samples_5ms = samplerate * 5 // 1000
    triangle = [float(x) / samples_5ms for x in range(samples_5ms)]

    for x in range(samples_5ms):
        wave[x] = wave[x] * triangle[x]

    for x in range(samples_5ms):
        wave[-x] = wave[-x] * triangle[x]

    return wave

def read_diphones(estfile):
    # read EST file:)
    with open(estfile, "r") as est:
        headerdone = False
        diphones = {}

        for l in est.readlines():
            # skip EST header
            if l.startswith("EST_Header_End"):
                headerdone = True
                continue
            if not headerdone:
                continue
            if len(l.strip())==0:
                continue

            # read diphone data
            diphone, rest = l.strip().split(None, 1)
            if rest.startswith("&"):
                # diphone alias
                diphones[diphone] = diphones[rest[1:]]
            else:
                # full diphone
                (wavfile, start, middle, end) = rest.split()
            (start, middle, end) = (float(start), float(middle), float(end))
            diphones[diphone] = (wavfile, start, middle, end)

    return diphones


def prob_gauss(x, o=1):
    return int(round(1000.0*math.exp(-x*x/(o*o*2.0)) / (o*math.sqrt(math.pi*2))))


def get_choice_list(lesson, learn, old=None, prefer_new=True):
    """
    :param lesson: Lesson number, starts with 1
    :param learn: Lesson order list
    :param old: How many old symbols to include (0 = just the current one)
    :return:
    """

    if old is None:
        old = lesson - 1

    choices = []
    for x in range(max(0, lesson - 1 - old), min(len(learn) - 1, lesson + 1)):
        if prefer_new:
            prob = prob_gauss(x - lesson, old * 2)
        else:
            prob = 1
        symbol = learn[x]
        print_v(1, f"Adding {mapa[symbol].text} prob: {prob}")
        choices.extend(prob*[mapa[symbol].text])
    return choices

def generate_sound_file(fname, diphones, wpm, eff_wpm, pitch, parts, repeat, groups, mode="w"):

    def _insert_morse_space(dits, destination):
        space_len = int(dits * dot_samples(eff_wpm, 44100))
        print_v(3, f"Space {dits} dits ({space_len} samples)")
        destination.extend([0] * space_len)

    # set position to first or last data byte in output file
    with wave.open(fname, mode+"b") as fout:
        fout.setnchannels(1)
        fout.setsampwidth(2)
        fout.setframerate(44100)
        # generate teach list
        # prepare all sounds

        for content in groups:
            for part in parts:
                samples = space(44100, 0.5)

                if part == "morse":
                    morse_group_data = []
                    for wrid, wr in enumerate(content):
                        if wr == " ":
                            w = MORSE_SPACE
                        elif wr == "":
                            continue
                        else:
                            w = mapa[wr]
                        print_v(2, w.morse)

                        # Remove the previous inter-character space if a proper space arrived
                        if w == MORSE_SPACE and morse_group_data and morse_group_data[-1][0] == False:
                            morse_group_data = morse_group_data[:-1]
                            print_v(3, "Retract extra space")

                        morse_data = morse(wpm, eff_wpm, w.morse, 44100)
                        print_v(3, morse_data)
                        morse_group_data.extend(morse_data)

                        if wrid < len(content) - 1 and w != MORSE_SPACE: # Do not add space after space
                            morse_group_data.append((False, 3 * dot_samples(eff_wpm, 44100)))

                    # Repeat the whole group
                    morse_samples = morse_audio(morse_group_data, 44100, pitch, 20000)
                    for n in range(repeat):
                        samples.extend(morse_samples)
                        if n < repeat - 1:
                            _insert_morse_space(3, samples)

                elif part == "voice":
                    for wr in content:
                        if wr == " ":
                            samples.extend(space(44100, 0.8))
                            continue
                        elif wr == "":
                            continue
                        else:
                            w = mapa[wr]
                        print_v(2, "".join(w.voice.split('/')))
                        samples.extend(word(diphones, w.voice))
                        samples.extend(space(44100, 0.1))

                # Space between parts (words)
                _insert_morse_space(7, samples)

                # Dump samples to file
                struct_format = "<%d%s" % (len(samples), STRUCTFORMAT[2])
                fout.writeframes(struct.pack(struct_format, *samples))

            # Extra space between groups
            samples = []
            _insert_morse_space(7, samples)
            struct_format = "<%d%s" % (len(samples), STRUCTFORMAT[2])
            fout.writeframes(struct.pack(struct_format, *samples))


def generate_phrase(phrase_line):
    """
    Take a phrase line like 'A <B C|1 0> | <D|E> F' and generate
    one random variant of the phrase split into groups.

    :param phrase_line:
    :return:
    """
    tokenize = re.split(r"([|<> ])", phrase_line)
    stack = [[""]]

    for t_raw in tokenize:
        t = t_raw.strip()
        if t == "|":
            stack[-1].append("")
            continue
        elif t == "<":
            stack.append([""])
        elif t == "_":
            stack[-1][-1] += " "
        elif t == ">":
            choices = stack[-1]
            stack = stack[:-1]
            stack[-1][-1] += random.choice(choices)
        else:
            stack[-1][-1] += t

    return [t for t in stack[0] if t]


if __name__ == "__main__":
    opts = docopt.docopt(__doc__)
    print(opts)

    verbosity = int(opts["--verbose"])

    if not any((opts["--cards"], opts["--text"], opts["--random"])):
        print(__doc__)
        sys.exit(1)

    tempo = int(opts["--speed"])
    pitch = int(opts["--pitch"])

    if opts["--phrases"] is not None:
        opts["--groups"] = 1

    learning_order = [t.text for t in table]
    if opts["--koch"]:
        learning_order = koch

    if opts["--print-lesson-name"]:
        sys.stdout.write(learning_order[int(opts["--lesson"]) - 1])
        sys.exit(0)

    if opts["--group"]:
        group_length = opts["--group"].split("-")
        if len(group_length) < 1 or len(group_length) > 2:
            print("Bad group length format: --group=int[-int]")
            sys.exit(1)

        try:
            group_length = [int(x) for x in group_length]
        except Exception:
            print("Bad group length format: --group=int[-int]")
            sys.exit(1)

        if len(group_length) == 1:
            group_length += group_length

    if opts["--random"]:
        teach = []
        if opts["--symbols"]:
            choices = opts["--symbols"].split()
        elif opts["--lesson"] is not None:
            choices = get_choice_list(int(opts["--lesson"]) - 1, learning_order, prefer_new=opts["--prefer-new"])
        elif opts["--phrases"]:
            with open(opts["--phrases"]) as phrase_file:
                choices = [r.strip().lower() for r in phrase_file.readlines() if r.strip() != ""]
        else:
            choices = list(mapa.keys())

        random_count = int(opts["--random"])
        while random_count > len(teach):
            if opts["--phrases"]:
                phrase_line = random.choice(choices)
                teach.extend(generate_phrase(phrase_line))
            else:
                group = []
                for c in range(random.randint(*group_length)):
                    group.append(random.choice(choices))
                teach.append(group)
    else:
        if opts["--symbols"]:
            teach = opts["--symbols"].split()
        elif opts["--lesson"] is not None:
            teach = learning_order[:int(opts["--lesson"])]
        else:
            teach = mapa.keys()

    estfile = os.path.join(opts["--voice"], "dic/phdiph.est")
    wavdir = os.path.join(opts["--voice"], "wav")
    if not opts["<dist>"]:
        opts["<dist>"] = "dist"

    try:
        os.makedirs(opts["<dist>"])
    except OSError:
        pass

    diphones = read_diphones(estfile)
    order = []
    if not opts["--morse"]:
        order.append("voice")

    if opts["--voice-first"]:
        order.append("morse")
    else:
        order.insert(0, "morse")

    if opts["--random"] or opts["--text"]:
        fname = os.path.join(opts["<dist>"], "dist.wav")
        print(f"Generating into {fname}")
        generate_sound_file(fname, diphones,
                            tempo, int(opts["--farnsworth"]), pitch, order, int(opts["--repeat"]),
                            teach)
    else: # cards
        for t in teach:
            fname = os.path.join(opts["<dist>"], "snd-" + mapa[t].name + ".wav")
            print(f"Generating into {fname}")
            generate_sound_file(fname, diphones,
                                tempo, int(opts["--farnsworth"]), pitch, order, int(opts["--repeat"]),
                                [t])
